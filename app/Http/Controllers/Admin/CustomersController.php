<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use DB;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['customers'] = DB::table('customers')->get();
        return view('admin.customers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'phoneNumber' => $request->phoneNumber,
            'gender' => $request->gender,
            'age' => $request->age,
        ]);
        $i = $customer->save();
        if ($i) {
            return redirect()->route('customer.create')
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('customer.create')
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['customer'] = Customer::find($id);
        return view('admin/customers/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phoneNumber' => 'required',
            'gender' => 'required',
            'age' => 'required',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phoneNumber' => $request->phoneNumber,
            'gender' => $request->gender,
            'age' => $request->age,
        );
        $i = DB::table('customers')->where('id', $id)->update($data);
        if ($i) {
            return redirect()->route('customer.edit', $id)
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('customer.edit', $id)
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $i = Customer::where('id', $id)->delete();
        if ($i) {
            if ($i) {
                return redirect()->route('customer.index')
                    ->with('success', 'Data has been deleted!');
            } else {
                return redirect()->route('customer.index')
                    ->with('error', 'Fail to delete data!');
            }
        }
    }
}
