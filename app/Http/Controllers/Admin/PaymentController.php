<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\PaymentType;
use DB;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['payment'] = DB::table('payment')
            ->join('customers', 'payment.id', '=', 'customers.id')
            ->join('payment_type', 'payment.id', '=', 'payment_type.id')
            ->select('payment.*', 'customers.name as customerName', 'payment_type.name as paymentTypeName')
            ->get();
        return view('admin.payment.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['paymentType'] = PaymentType::all();
        $data['customers'] = DB::table('customers')->get();
        return view('admin/payment/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $payment = Payment::create([
            'amount' => $request->amount,
            'customerId' => $request->customerId,
            'paymentTypeId' => $request->paymentTypeId,
            'paymentDate' => $request->paymentDate,
        ]);
        $i = $payment->save();
        if ($i) {
            return redirect()->route('payment.create')
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('payment.create')
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
