<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoomType;
use DB;

class RoomTypeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index()
  {
    $data['roomTypes'] = RoomType::all();
    return view('admin/room-type/index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function create()
  {
    return view('admin/room-type/create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function store(Request $request)
  {
    $roomType = RoomType::create([
      'name' => $request->name,
      'type' => $request->type,
    ]);
    $i = $roomType->save();
    if ($i) {
      return redirect()->route('roomType.create')
        ->with('success', 'Data has been saved!');
    } else {
      return redirect()->route('roomType.create')
        ->with('error', 'Fail to save data!');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function edit($id)
  {
    $data['roomType'] = RoomType::find($id);
    return view('admin/room-type/edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(Request $request, $id)
  {
    $request->validate([
      'name' => 'required',
      'type' => 'required',
    ]);
    $data = array(
      'name' => $request->name,
      'type' => $request->type,
    );
    $i = DB::table('room_types')->where('id', $id)->update($data);
    if ($i) {
      return redirect()->route('roomType.edit', $id)
        ->with('success', 'Data has been saved!');
    } else {
      return redirect()->route('roomType.edit', $id)
        ->with('error', 'Fail to save data!');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function delete($id)
  {
    $i = RoomType::where('id', $id)->delete();
    if ($i) {
      if ($i) {
        return redirect()->route('roomType.index')
          ->with('success', 'Data has been deleted!');
      } else {
        return redirect()->route('roomType.index')
          ->with('error', 'Fail to delete data!');
      }
    }
  }
}
