<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\RoomType;
use DB;

class RoomController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index()
  {
    $data['rooms'] = DB::table('rooms')
      ->join('room_types', 'rooms.roomTypeId', '=', 'room_types.id')
      ->join('branches', 'rooms.branchId', '=', 'branches.id')
      ->select('rooms.*', 'room_types.name as roomType', 'branches.branchName as branch')
      ->get();
    return view('admin/room/index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function create()
  {
    $data['roomTypes'] = RoomType::all();
    $data['branches'] = DB::table('branches')->get();
    return view('admin/room/create', $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function store(Request $request)
  {
    $room = Room::create([
      'roomNo' => $request->roomNo,
      'roomTypeId' => $request->roomTypeId,
      'rate' => $request->rate,
      'price' => $request->price,
      'branchId' => $request->branchId,
      'adult' => $request->adult,
      'child' => $request->child,
    ]);
    $i = $room->save();
    if ($i) {
      return redirect()->route('room.create')
        ->with('success', 'Data has been saved!');
    } else {
      return redirect()->route('room.create')
        ->with('error', 'Fail to save data!');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function edit($id)
  {
    $data['room'] = Room::find($id);
    $data['roomTypes'] = RoomType::all();
    $data['branches'] = DB::table('branches')->get();
    return view('admin/room/edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(Request $request, $id)
  {
    $request->validate([
      'roomNo' => 'required',
      'roomTypeId' => 'required',
      'price' => 'required',
      'branchId' => 'required',
      'adult' => 'required',
      'child' => 'required',
    ]);

    $data = array(
      'roomNo' => $request->roomNo,
      'roomTypeId' => $request->roomTypeId,
      'rate' => $request->rate,
      'price' => $request->price,
      'branchId' => $request->branchId,
      'adult' => $request->adult,
      'child' => $request->child,
    );
    $i = DB::table('rooms')->where('id', $id)->update($data);
    if ($i) {
      return redirect()->route('room.edit', $id)
        ->with('success', 'Data has been saved!');
    } else {
      return redirect()->route('room.edit', $id)
        ->with('error', 'Fail to save data!');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\RedirectResponse
   */
  public function delete($id)
  {
    $i = Room::where('id', $id)->delete();
    if ($i) {
      if ($i) {
        return redirect()->route('room.index')
          ->with('success', 'Data has been deleted!');
      } else {
        return redirect()->route('room.index')
          ->with('error', 'Fail to delete data!');
      }
    }
  }
}
