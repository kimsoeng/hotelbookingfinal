### HOTEL BOOKING


## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)


Clone the repository

    git clone https://gitlab.com/kimsoeng/ormhotelwebsite.git

Switch to the repo folder

    cd ormhotelwebsite

Switch to dev branch

    git checkout dev

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate


Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Run the database seed 

    composer dump-autoload

    php artisan db:seed

Start the local development server

    php artisan serve
    
Default username and password for logging into admin dashboard

    - Email: admin@gmail.com
    - Password: 12345678

You can now access the server at http://127.0.0.1:8000



