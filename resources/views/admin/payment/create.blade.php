@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Create Payment</h1>
    <form action="{{route('payment.store')}}" method="POST">
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{url('payment')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          <div>
            {{csrf_field()}}
              <div class="form-group row">
                  <label for="customerId" class="col-sm-4 form-control-label">Customer ID</label>
                  <div class="col-sm-8">
                      <select class="form-control" name="customerId" id="customerId">
                          <option value="">select customer</option>
                          @foreach($customers as $customer)
                              <option id="customerId" name="customerId" value="{{$customer->id}}">{{$customer->name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group row">
                  <label for="customerId" class="col-sm-4 form-control-label">Payment Type ID</label>
                  <div class="col-sm-8">
                      <select class="form-control" name="paymentTypeId" id="paymentTypeId">
                          <option value="">select payment</option>
                          @foreach($paymentType as $p)
                              <option id="paymentTypeId" name="paymentTypeId" value="{{$p->id}}">{{$p->name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group row">
                <label for="Name" class="col-sm-4 form-control-label">Amount <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="Enter your payment amount" value="{{old('amount')}}" required autofocus>
                </div>
              </div>
              <div class="form-group row">
                  <label for="Name" class="col-sm-4 form-control-label">Date <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                      <input type="date" class="form-control" id="paymentDate" name="paymentDate" value="{{old('paymentDate')}}" required autofocus>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection

@section('js')
<script>
  // Disable form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@endsection
