@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Edit Customer</h1>
    <form action="{{route('customer.update', $customer->id)}}" method="POST">
      {{ method_field('PUT') }}
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{url('customer')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          <div>
            {{csrf_field()}}
            <div class="form-group row">
              <label for="Name" class="col-sm-4 form-control-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter your Name" value="{{$customer->name}}" required autofocus>
              </div>
            </div>
            <div class="form-group row">
              <label for="Type" class="col-sm-4 form-control-label">Email <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="email" class="form-control" id="email" name="email" value="{{$customer->email}}" required autofocus>
              </div>
            </div>
            <div class="form-group row">
              <label for="Type" class="col-sm-4 form-control-label">Phone Number <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" value="{{$customer->phoneNumber}}" required autofocus>
              </div>
            </div>
            <div class="form-group row">
              <label for="Type" class="col-sm-4 form-control-label">Gender <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <select class="form-control" value="{{$customer->gender}}" name="gender" id="gender">
                        <option id="gender" value="{{$customer->gender}}">{{$customer->gender}}</option>
                        <option id="gender" value="male" name="male">Male</option>
                        <option id="gender" value="female" name="female">Female</option>
                    </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="Type" class="col-sm-4 form-control-label">Age <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="number" class="form-control" id="age" name="age" value="{{$customer->age}}" required autofocus>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection
