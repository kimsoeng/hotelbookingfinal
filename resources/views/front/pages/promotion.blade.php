@extends('layouts.front')
@section('style')
<style> 
    .banner-section {
        background-image: url('{{ url("https://sokhahotels.com.kh/img/newslideshows/Swimming-pool-.jpg") }}');
        background-repeat: no-repeat;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        height: 565px;
    }
</style>
@endsection
@section('banner')
<!-- <div class="col-md-2"></div> -->
<div>
    <div class="col-md-12 justify-content-center text-center" style="margin-top: 200px;">
        <h1 class="text-white" style="font-size:62px"><strong> Phnom Penh</strong></h1>
        <p style="font-size: 20px; color: #1CC3B2"><strong> <i class="fa fa-phone" style="font-size: 25px;"></i> 098 65 78 656</strong></p>
        <p style="font-size: 20px; color: #1CC3B2"><strong> <i class="fa fa-envelope-o" style="font-size: 25px;"></i> hotel@gmail.com</strong></p>
    </div>

</div>
@section('content')
<div class='col-md-12' style="padding: 20px;">
    <h2>A Collection Of True Khmer Hospitality</h2>
    <h4>About Hotels & Resorts</h4>
    <p>
        The Sokha Hotels & Resorts also known as Sokha Hotel Group is based in Phnom Penh and it is also Cambodian
        owned company which is under the Sokimex Investment Group. The company was founded in 2004 by Neak Oknha Sok Kong.
        Today, Sokha Hotels & Resort has seven (07) hotel properties located in four (04) main cities and provinces in Cambodia.
        As the largest Cambodia Hospitality Operator, SOKHA is well known of its 5-star Cambodia service - a Cambodia proud.
        Our success is driven by our people and their commitment to reach the mission of providing guests the most memorable
        experience and unforgettable smile of Cambodian.
    </p>
    <div class="row">
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
        <div class="col-md-2">
            <h4 style="text-align: center;">Special Offers</h4>
        </div>
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
    </div>

    <div class="container-fluid con-gallery" style="margin-top: 30px;">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ url("https://www.orussey1.com/userfiles/thumbs/6u7b2128_29_30_tonemapped(1).jpg") }}" class="rounded" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Greate Weekday</h5>
            </div>
            <div class="col-md-4">
                <img src="{{ url("https://i.ytimg.com/vi/ZVOEMhcaglo/maxresdefault.jpg") }}" class="rounded" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Explore Tour</h5>
            </div>
            <div class="col-md-4">
                <img src="{{ url("https://sokhahotels.com.kh/img/special-offers/SAHS-0014(Micro-Site-2013)_02-Early-Bird-Booking-1.jpg") }}" class="rounded" width="100%" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Early promotion 30% off by booking from website</h5>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
        <div class="col-md-2">
            <h4 style="text-align: center;">Our Rooms</h4>
        </div>
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
    </div>

    <div class="container-fluid con-gallery" style="margin-top: 30px;">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ url("https://www.mydomaine.com/thmb/tuU-BjCzJlV3XMP-UsYpZ4BixYM=/2375x1473/filters:fill(auto,1)/DesignbyEmilyHendersonDesignPhotographerbySaraTramp_202-4539d306a46d454abd97643b3d240a90.jpg") }}" class="rounded" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Double Beds Room</h5>
            </div>
            <div class="col-md-4">
                <img src="{{ url("https://pix10.agoda.net/hotelImages/916117/-1/bb81be20a7c72efa182ed067eaef7523.jpg?s=1024x768") }}" class="rounded" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Single Bed Room</h5>
            </div>
            <div class="col-md-4"> 
                <img src="{{ url("https://www.regalhotel.com/uploads/rrh/accommodations/720x475/Family-Triple-Room-2.jpg") }}" class="rounded" width="100%" width="100%" style="height: 230;">
                <h5 style="text-align: center;">Triple Beds Room</h5>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
        <div class="col-md-2">
            <h4 style="text-align: center;">Dining</h4>
        </div>
        <div class="col-md-5">
            <hr style="border-color: #1CC3B2 ">
        </div>
    </div>

    <div class="container-fluid con-gallery" style="margin-top: 30px;">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ url("https://133777-1202060-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2016/07/Bumbu-Bali-Nusa.jpg") }}" class="rounded" width="100%" style="height: 230;">
            </div>
            <div class="col-md-3">
                <img src="{{ url("https://www.alpenklang.info/wp-content/uploads/hotel-alpenklang-inklusivleistungen-kulinarik-2-1024x683.jpg") }}" class="rounded" width="100%" style="height: 230;">
            </div>
            <div class="col-md-3">
                <img src="{{ url("https://miro.medium.com/max/800/1*2v0XHEuiEXmvy216TJiRYw.jpeg") }}" class="rounded" width="100%" width="100%" style="height: 230;">
            </div>
            <div class="col-md-3">
                <img src="{{ url("https://media-cdn.tripadvisor.com/media/photo-s/13/b7/91/dd/tap-tandoor-food.jpg") }}" class="rounded" width="100%" width="100%" style="height: 230;">
            </div>
        </div>
    </div>
</div>

@endsection