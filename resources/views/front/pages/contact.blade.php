@extends('layouts.front')
@section('style')
<style>
  .banner-section {
    background-image: url('{{ url("storage/images/banner_contact.jpg") }}');
    background-repeat: no-repeat;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 565px;
  }
</style>
@endsection
@section('banner')
<div class="col-md-2"></div>
<div class="col-md-5" style="margin-top: 200px;">
  <h1 class="text-white" style="font-size:65px"><strong><i class="fa fa-phone" aria-hidden="true" style="font-size: 90px;"></i> Contact Us</strong></h1>
  <p class="text-white" style="font-size: 25px;"><i class="fa fa-phone" style="font-size: 40px;"></i> 098 65 78 656</p>
  <p class="text-white" style="font-size: 25px;"><i class="fa fa-envelope-o" style="font-size: 40px;"></i> hotel@gmail.com</p>
</div>
<div class="col-md-4" style="margin-top: 120px;">
  <div class="panel">
    <div class="panel-body">
      <h2>Message</h2>
      <form action="#">
        <div class="form-group"><input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full Name"></div>
        <div class="form-group"><input type="email" class="form-control" name="email" id="email" placeholder="Email"></div>
        <div class="form-group"><input type="number" class="form-control" name="phone" id="phone" placeholder="Phone number"></div>
        <div class="form-group"><textarea name="message" id="message" cols="47" rows="5" placeholder="Message" style="margin: 0px; width: 100%"></textarea></div>
        <div class="form-group"><button class="btn bg-brand btn-block text-white"><strong>Send Now</strong></button></div>
      </form>
    </div>
  </div>
</div>
<div class="col-md-1"></div>

@endsection