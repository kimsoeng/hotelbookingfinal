@extends('layouts.front')
@section('style')
<style>
  .banner-section {
    background-image: url('{{ url("storage/images/banner_home.jpg") }}');
    background-repeat: no-repeat;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: auto;
  }
  .label-price {font-size: 24px;}
  .show {display: block;}
  .room-price {font-size: 20px;}
  .text-discount {text-decoration: line-through;}
  .room-col {margin-top: 20px;}
  .con-left {box-shadow: 0 4px 8px 0 rgba(8, 8, 8, 0.06), 0 6px 20px 0 rgba(8, 8, 8, 0.06);}
  .card-booking {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 3px;
    padding: 45px;
  }
</style>
@endsection
@section('banner')
@include("front.banner")
@endsection
@section('content')
<div class="container-fluid" id="main-booking">
  <h1 class="text-center"><strong> Stays in Krong Siem Reap</strong></h1><br>
  <div class="row">
    <div class="col-md-3 con-left text-white">
      <a href="#main-booking" id="book-details">
        <div class="row room-col">
          <div class="col-md-6"><img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%"></div>
          <div class="col-md-6">
            <span class="text-gray">Private room in Krong Siem Reap</span><br><br>
            <p>Open Air Rooftop Bungalow</p>
            <span class="text-gray">2 guests · 1 bedroom · 1 bed · 1 private bath</span><br><br><br>
            <p class="pull-right room-price"><strong class="text-discount text-gray">$7</strong><strong> $6</strong> / night</p>
          </div>
        </div>
      </a>
      <hr>
      <a href="#main-booking">
        <div class="row room-col">
          <div class="col-md-6"><img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%"></div>
          <div class="col-md-6">
            <span class="text-gray">Queen room in Krong Phnom Penh</span><br><br>
            <p>Open Air Rooftop Bungalow</p>
            <span class="text-gray">2 guests · 1 bed · 1 private bath</span><br><br><br>
            <p class="pull-right room-price"><strong class="text-discount text-gray">$8</strong><strong> $7</strong> / night</p>
          </div>
        </div>
      </a>
      <hr>
      <a href="#main-booking">
        <div class="row room-col">
          <div class="col-md-6"><img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%"></div>
          <div class="col-md-6">
            <span class="text-gray">King room in Krong Phnom Penh</span><br><br>
            <p>Open Air Rooftop Bungalow</p>
            <span class="text-gray">2 guests · 1 bed · 1 private bath</span><br><br><br>
            <p class="pull-right room-price"><strong class="text-discount text-gray">$8</strong><strong> $7</strong> / night</p>
          </div>
        </div>
      </a><br>
    </div>
    <div class="col-md-9 con-booking-detail hide" style="padding: 0 30px;">
      <div class="row">
        <div class="col-md-6">
          <img src="{{ url("storage/images/gallery_1.jpg") }}" width="100%">
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6 clear-padding-right">
              <img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%">
            </div>
            <div class="col-md-6 clear-padding-right">
              <img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6 clear-padding-right">
              <img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%">
            </div>
            <div class="col-md-6 clear-padding-right">
              <img src="{{ url("storage/images/gallery_1.jpg") }}" class="img-rounded" width="100%">
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-6 col-md-6">
          <h3>Private room in Krong Siem Reap</h3>
          <span class="text-gray">2 guests · 1 bedroom · 1 bed · 1 private bath</span>
          <br><br>
          <p style="font-size:15px"><i class="fa fa-calendar"></i> Free cancellation until 1:00 PM on Jul 21 After that, cancel before 1:00 PM on Jul 22 and get a full refund, minus the first night and service fee.</p>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-6 col-md-6">
          <div class="card card-booking">
            <p class="label-price"><strong class="text-discount text-gray">$8</strong><strong> $7</strong> / night</p><br>
            <div class="card-body">
              <form>
                <div class=" form-group">
                  <label for="checkInDate" class="text-gray">Check In Date: </label>
                  <input type="date" class="form-control" id="checkInDate" value="2020-08-17">
                </div>
                <div class=" form-group">
                  <label for="checkOutDate" class="text-gray">Check Out Date: </label>
                  <input type="date" class="form-control" id="checkOutDate" value="2020-08-19">
                </div>
                <div class=" form-group">
                  <label for="adult" class="text-gray">Adult: </label>
                  <select class="form-control" id="adult">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                  </select>
                </div>
                <div class=" form-group">
                  <label for="children" class="text-gray">Children: </label>
                  <select class="form-control" id="children">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                  </select>
                </div>
                <div class="row" style="font-size: 17px;padding-left: 16px;">
                  <button type="button" class="btn bg-brand btn-block text-white" id="btn-reserve" data-toggle="modal" data-target="#exampleModal"><strong style="font-size: 16px"> Process To Book</strong></button><br>
                  <p><span>$7 x 3 nights</span><span class="pull-right">$21</span></p>
                  <p><span>Cleaning fee</span><span class="pull-right">$5</span></p>
                  <p><span>Service fee</span><span class="pull-right">$5</span></p>
                  <hr>
                  <p><strong>Total</strong> <span class="pull-right">$31</span></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel"><strong>Booking</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <p><strong>Order Info</strong><span class="pull-right text-brand"><strong>Total: $21 </strong></span></p>
          <p class="text-gray">
            Private room with one bed - 3 Days <br>
            Booking Date: 2020-08-17 - 2020-08-19 <br>
            Person: 1 + 1 child <br>
          </p>
          <p class="pull-right text-gray">(Sime Reap)</p><br>
          <div class="info-modal-content">
            <h5 class="text-gray"><strong>Personal Info</strong></h5>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="fullName">Full Name</label>
                  <input type="text" class="form-control" name="fullName" id="fullName" value="Kimsoeng Kao" readonly>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" id="email" value="kimsoeng.kao@gmail.com" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="country">Country</label>
                  <input type="text" class="form-control" name="country" id="country" value="Cambodia" readonly>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group ">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" name="address" id="address" value="Solar, 371, Tropang CHHUK" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="phone">Phone number</label>
                <input type="number" class="form-control" name="phone" id="phone" value="09374756" readonly>
              </div>
            </div>
          </div>
          <div class="payment-modal-content hide">
            <h5 class="text-gray"><strong>Payment Method</strong></h5>
            <div class="row">
              <div class="col-md-6"><input type="number" class="form-control" name="cardNumber" id="cardNumber" placeholder="card number"></div>
              <div class="col-md-6"><input type="date" class="form-control" name="dateExp" id="dateExp"></div>
              <div class="col-md-6"><br><input type="number" class="form-control" name="cvv" id="cvv" placeholder="CVV"></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn bg-brand text-white btn-block" id="btn-continue-payment">Continue to payment</button>
        <button type="button" class="btn bg-brand text-white btn-block hide" id="btn-confirm-payment">Confirm Payment</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
<script>
  $(function() {
    $('#btn-continue-payment').click(function() {
      $('.info-modal-content').removeClass('show').addClass('hide');
      $('.payment-modal-content').removeClass('hide').addClass('show');
      $('#btn-continue-payment').removeClass('show').addClass('hide');
      $('#btn-confirm-payment').removeClass('hide').addClass('show');
    });
    $(".modal").on("hidden.bs.modal", function() {
      $(this).find('form').trigger('reset');
      $('.info-modal-content').removeClass('hide').addClass('show');
      $('.payment-modal-content').removeClass('show').addClass('hide');
      $('#btn-continue-payment').removeClass('hide').addClass('show');
      $('#btn-confirm-payment').removeClass('show').addClass('hide');
    });
    $('#btn-confirm-payment').click(function() {
      $("#exampleModal").modal("hide");
    });
    $('#book-details').click(function() {
      $(".con-booking-detail").removeClass('hide').addClass("show")
    });
  })
</script>
@endsection