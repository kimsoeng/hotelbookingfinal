@extends('layouts.front')
@section('style')
<style>
  .con-gallery {
    margin-top: 250px;
    background: rgb(39, 39, 39);
    padding: 51px;
  }

  .rounded {
    border-radius: 5px;
    box-shadow: 0 4px 8px 0 rgba(245, 243, 243, 0.09), 0 6px 20px 0 rgba(245, 243, 243, 0.09);
  }
</style>
@endsection
@section('banner')
@include("front.banner")
@endsection
@section('content')
<div class="container">
  <div class="row">
    <h1 class="text-brand text-center"><strong>ABOUT US</strong></h1><br>
    <p>An enterprise of Select Group with interests in core Hospitality, Travel and Retail, amongst others, Select Hotels owns and operates two chic
      urban resorts in the country offering superlative experiences and one boutique hotel located in a quaint neighbourhood of New Delhi offering superior experiences for corporate meetings and social gatherings. Select Hotels is dedicated to guest service, attention to detail and an uncompromising commitment to quality. Over the years, the Select Hotels brand has built a reputation for excellence in accommodation, food & beverage and service. The high standards of quality – from the architecture to interiors, landscaping to facilities offered, impeccable service to delicious cuisines – are as discerning as guests have come to expect from it. Both resorts under the Select Hotels brand continuously strive for excellence
      to set new standards in the quality of their services and facilities.</p>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5"><img src="{{ url("storage/images/mission.jpg") }}" alt="misson" width="100%"></div>
    <div class="col-md-5">
      <h2>Mission</h2>
      <p>urban resorts in the country offering superlative experiences and one boutique hotel located in a
        quaint neighbourhood of New Delhi offering superior experiencesurban resorts in the country offering superlative experiences and one boutique hotel located in a
        quaint neighbourhood of New Delhi offering superior experiences</p>
    </div>
    <div class="col-md-1"></div>
  </div>
  <br><br><br><br><br><br>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
      <h2>Mission</h2>
      <p>urban resorts in the country offering superlative experiences and one boutique hotel located in a
        quaint neighbourhood of New Delhi offering superior experiencesurban resorts in the country offering superlative experiences and one boutique hotel located in a
        quaint neighbourhood of New Delhi offering superior experiences</p>
    </div>
    <div class="col-md-5"><img src="{{ url("storage/images/vision.jpg") }}" alt="misson" width="100%"></div>
    <div class="col-md-1"></div>
  </div>
</div>
<div class="container-fluid con-gallery">
  <h1 class="text-white text-center">Gallery</h1><br><br>
  <div class="row">
    <div class="col-md-3"><img src="{{ url("storage/images/hotel/room.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/gallery_1.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/room_2.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/room_3.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-3"><img src="{{ url("storage/images/room_4.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/room_5.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/room_6.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
    <div class="col-md-3"><img src="{{ url("storage/images/room_7.jpg") }}" class="rounded" width="100%" style="max-height: 145px;"></div>
  </div>
</div>
@endsection