@extends('layouts.front')
@section('style')
<style>
  .banner-section {
    background-image: url('{{ url("storage/images/banner_contact.jpg") }}');
    background-repeat: no-repeat;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 400px;
  }
  .con-career{
    border-radius: 5px;
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 10px;
  }
</style>
@endsection
@section('banner')
<div class="container">
  <div class="row" style="margin-top: 150px;">
    <h1 class="text-white" style="font-size:109px"><strong> Careers</strong></h1>
    <p class="text-white" style="font-size: 40px;"><strong>Join Our Team</strong></p>
  </div>
</div>
@endsection
@section('content')
<br><br>
<div class="container">
    <div class="col-md-12 con-career" id="con-career">
      <div class="row">
        <h2 class="text-center">Designer</h2>
      </div>
  </div>
</div>
<br><br>
@endsection