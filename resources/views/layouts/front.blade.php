<!DOCTYPE html>
<html lang="en">

<head>
  <title>Hotel | Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{ url('storage/images/main/orkide.png') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Battambang|Koulen&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL::asset('css/front.css')}}">

  <style>
    .banner-section {
      background-image: url('{{ url("storage/images/banner_home.jpg") }}');
      background-repeat: no-repeat;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      height: auto;
    }

    .banner-section {
      margin-right: 0;
    }

    .col-md-1 {
      width: 4.333333%;
    }
  </style>
  @yield('style')

</head>

<body class="background-body-image">

  {{--Menu Section--}}
  <nav class="navbar navbar-default navbar-fixed-top clear-padding" style="border: none; background: none;">
    <div class="container-fluid middle-header">
      <div class="container" style="padding: 5px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('home') }}">
            <i class="fa fa-h-square text-brand fa-3x"></i><strong class="text-brand" style="font-size: 34px;">otel</strong>
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1" style="margin-top: 6px;">
          <ul class="nav navbar-nav navbar-right">
            <li class="active link"><a href="{{ url('home') }}" class="header-nav-link">@lang("HOME")</a></li>
            <li class="link"><a href="{{ url('about') }}" class="header-nav-link">@lang("ABOUT US")</a></li>
            <li class="link"><a href="{{ url('booking') }}" class="header-nav-link">@lang("BOOKING")</a></li>
            <li class="link"><a href="{{ url('promotion') }}" class="header-nav-link">@lang("SPECIAL OFFER")</a></li>
            <li class="link"><a href="{{ url('contact') }}" class="header-nav-link">@lang("CONTACT US")</a></li>
            <li class="link"><a href="{{ url('career') }}" class="header-nav-link">@lang("CAREER")</a></li>
            @guest
            <li class="link"><a class="header-nav-link" href="{{ route('login') }}"><i class="fa fa-sign-in"></i> {{ __('Login') }}</a></li>
            @endguest
            @auth
            <li><a class="header-nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out pull-right"></i> LogOut</a>
            </li>
            <li class="pull-right"><a class="header-nav-link" href="{{ url('admin') }}"><i class="fa fa-tachometer text-brand"></i></a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
            @endauth
          </ul>
        </div>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  {{--End Menu Section--}}

  {{-- banner --}}
  <div class="conterin-fluid clear-padding" id="banner">
    <div class="banner-section row">
      @yield('banner')
    </div>
  </div>
  {{-- End banner --}}

  {{--body--}}
  <div class="container-fluid clear-padding" id="main-container" style="margin:20px 0 20px  0px">

    @yield('content')

  </div>
  {{--end body--}}
  {{--Footer--}}
  <div class="container-fluid">
    <div class="row section-map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.0527841172734!2d104.91711721548545!3d11.548071391800725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951216e7aaaab%3A0xbc67a2b632a5446!2sSokha%20Hotel%20Phnom%20Penh!5e0!3m2!1sen!2skh!4v1594888453696!5m2!1sen!2skh" width="100%" height="380" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <div class="row footer-bottom">
      <br>
      <div class="container">
        <div class="row">
          <ul class="footer-end">
            <a href="#">
              <li><strong>© Hotel 2008 - 2020</strong></li>
            </a>
            <a href="#">
              <li>Terms and Conditions</li>
            </a>
            <a href="#">
              <li>Privacy Policy</li>
            </a>
          </ul>
        </div>
      </div>
    </div>
  </div>
  {{--End Footer--}}

  <script>
    var bannerHeight = $('#banner').height();
    (function($) {
      $('.dropdown-submenu a.submenu').on("click", function(e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
      });

      //Set active menu
      var url = window.location.href;
      // passes on every "a" tag
      $(".menu a").each(function() {
        // checks if its the same on the address bar
        if (url == (this.href)) {
          $('li').removeClass('active');
          $(this).closest("li").addClass("active");
        }
      });

      function setDefaultMenu() {
        $(".middle-header").css({
          "background-color": "rgba(0, 0, 0, 0.5)"
        });
        $(".navbar-default .navbar-nav>li>a").css("color", "white");
      }

      function changeMenu() {
        $(".middle-header").css({
          "background-color": "white"
        });
        $(".navbar-default .navbar-nav>li>a").css("color", "#1cc3b2");
      }

      $(window).scroll(function(event) {
        event.preventDefault();
        var scroll = $(window).scrollTop();

        //bigger than 1000: show middle header, change navbar color background
        if (scroll > bannerHeight) {
          changeMenu();
        } else {
          setDefaultMenu();
        }
      });
    })(jQuery);

    $(function() {
      function scrollToAnchor(aid) {
        $('html,body').animate({
          scrollTop: $(aid).offset().top - 52
        }, 'slow');
      }
      $(".link-inside-menu").click(function(e) {
        e.preventDefault();
        scrollToAnchor($(this).attr("href"));
      });
    })
  </script>

  @yield('js')

</body>

</html>