<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- Bootstrap CSS CDN -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
   <!-- Our Custom CSS -->
   <link rel="stylesheet" href="{{ URL::asset('css/admin.css')}}">
   <!-- Font Awesome JS -->
   <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
   <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
   </link>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   </link>
   <style>
      .wrapper {
         display: flex;
         align-items: stretch;
      }

      #sidebar {
         min-width: 250px;
         max-width: 250px;
         min-height: 120vh;
      }

      #sidebar.active {
         margin-left: -250px;
      }

      a[data-toggle="collapse"] {
         position: relative;
      }

      .dropdown-toggle::after {
         display: block;
         position: absolute;
         top: 50%;
         right: 20px;
         transform: translateY(-50%);
      }

      @media (max-width: 768px) {
         #sidebar {
            margin-left: -250px;
         }

         #sidebar.active {
            margin-left: 0;
         }
      }

      @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";

      body {
         font-family: 'Poppins', sans-serif;
         background: #fafafa;
      }

      p {
         font-family: 'Poppins', sans-serif;
         line-height: 1.7em;
         color: #999;
      }

      a,
      a:hover,
      a:focus {
         color: inherit;
         text-decoration: none;
         transition: all 0.3s;
      }

      #sidebar {
         /* don't forget to add all the previously mentioned styles here too */
         background: #202429;
         color: #fff;
         transition: all 0.3s;
      }

      #sidebar .sidebar-header {
         padding: 20px;
         background: #1cc3b2;
      }

      #sidebar ul.components {
         padding: 20px 0;
         border-bottom: 1px solid #47748b;
      }

      #sidebar ul p {
         color: #fff;
         padding: 10px;
      }

      #sidebar ul li a {
         padding: 10px 30px;
         font-size: 14px;
         display: block;
      }

      #sidebar ul li a:hover {
         color: white;
         background: #474545;
      }

      #sidebar ul li.active>a,
      a[aria-expanded="true"] {
         color: #fff;
         background: #474545;
      }

      .content {
         width: 90%;
         margin: 2% 5%;
      }

      .sidebar__title {
         text-align: center;
      }
   </style>
   @yield('style')
</head>

<body>
   <div style="display: flex;">
      <div id="wrapper">
         <!-- Sidebar -->
         <nav id="sidebar">
            <div class="sidemenu">
               <ul class="list-unstyled components">
                  <div style="text-align:center">
                     <a class="navbar-brand" href="{{ url('admin') }}">
                        <i class="fa fa-h-square text-brand fa-3x"></i><strong class="text-brand" style="font-size: 34px;">otel</strong>
                     </a>
                  </div>
                  <hr>
                  <li class="active">
                     <a href="/admin"><i class="fa fa-user-friends"></i> &nbsp; Users</a>
                  </li>
                  <li>
                     <a href="/customer"><i class="fa fa-users"></i> &nbsp; Customers</a>
                  </li>
                  <li>
                     <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-home"></i> &nbsp; Room</a>
                     <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                           <a href="{{route('room.index') }}">Room</a>
                           <a href="{{route('roomType.index') }}">Room Type</a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="#payment" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-dollar-sign"></i> &nbsp; Payment</a>
                     <ul class="collapse list-unstyled" id="payment">
                        <li>
                           <a href="{{url('/payment') }}">Payment</a>
                        </li>
                        <li>
                           <a href="{{url('/paymentType') }}">Payment Type</a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="/branch"> <i class="fa fa-location-arrow"></i> &nbsp;Branch</a>
                  </li>
                  <li>
                     <a href="/contactus"><i class="fa fa-address-card"></i> &nbsp;Contact</a>
                  </li>
                   @auth
                       <li><a class="header-nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                               <i class="fa fa-power-off"></i> LogOut</a>
                       </li>
                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                           {{ csrf_field() }}
                       </form>
                   @endauth
               </ul>
            </div>
         </nav>
      </div>
      <div class=" main" style="width: 100%;">
         <div class="content">
            <!-- content  -->
            @yield('content')
         </div>
      </div>

      <!-- /#page-wrapper -->
      <!-- jQuery CDN - Slim version (=without AJAX) -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <!-- Popper.JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
      <!-- Bootstrap JS -->
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script>
         $(document).ready(function() {

            $('#sidebarCollapse').on('click', function() {
               $('#sidebar').toggleClass('active');
            });
            //Set active menu
            var url = window.location.href;
            $(".sidemenu a").each(function() {
               // checks if its the same on the address bar
               if (url == (this.href)) {
                  $('li').removeClass('active');
                  $(this).closest("li").addClass("active");
               }
            });

         });

         $(document).ready(function() {
            $('#example').DataTable();
         });
      </script>
      @yield('js')
</body>

</html>
