<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Admin routes
Route::group(['middleware' => 'auth'], function () {

  Route::get('/admin', 'AdminController@index');
  Route::get('/rooms', 'AdminController@rooms');
  Route::get('/report', 'AdminController@report');
  Route::get('/contactus', 'AdminController@contact');
  Route::get('/admin/create', 'AdminController@createUser');
  Route::get('/admin/eidt', 'AdminController@editUser');
  Route::resource('roomType', 'Admin\RoomTypeController')->except('destroy', 'show');
  Route::get('/roomType/delete/{id}', 'Admin\RoomTypeController@delete');
  Route::get('/admin/create', 'AdminController@create');
  Route::post('/admin/save', 'AdminController@save');
  Route::get('/admin/delete', 'AdminController@delete');
  Route::get('/admin/edit/{id}', 'AdminController@edit');
  Route::post('/admin/update/{id}', 'AdminController@update');
  // customer
  Route::resource('customer', 'Admin\CustomersController')->except('destroy', 'show');
  Route::get('/customer/delete/{id}', 'Admin\CustomersController@delete');
  // branch
  Route::resource('branch', 'Admin\BranchController')->except('destroy', 'show');
  Route::get('/branch/destroy/{id}', 'Admin\BranchController@destroy');
  //room
  Route::resource('room', 'Admin\RoomController')->except('destroy', 'show');
  Route::get('/room/delete/{id}', 'Admin\RoomController@delete');
  // payment
  Route::resource('payment', 'Admin\PaymentController')->except('destroy', 'show');
  Route::get('/payment/delete/{id}', 'Admin\PaymentController@delete');
  // payment type
  Route::resource('paymentType', 'Admin\PaymentTypeController');
  Route::get('/paymentType/delete/{id}', 'Admin\PaymentTypeController@delete');
});

Auth::routes();

//Front routes
Route::get('/', 'FrontController@index')->name('home');
Route::get('/home', 'FrontController@index')->name('home');
Route::get('/about', 'FrontController@about');
Route::get('/booking', 'FrontController@booking');
Route::get('/contact', 'FrontController@contact');
Route::get('/career', 'FrontController@career');
Route::get('/career-detail', 'FrontController@careerDetail');
Route::get('/promotion', 'FrontController@promotion');


Route::get('my-users', 'HomeController@myUsers');
