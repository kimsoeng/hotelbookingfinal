@extends('layouts.admin')
@section('style')
<style>
   .users__btn__add {
   margin-bottom: 30px;
   }
   .users__table {
   width:100%;
   }
   .item__action {
   width: 140px;
   }
   .item__action__btn {
   display:flex;
   justify-content:space-between;
   }
   .form__btn {
      display: flex;
      justify-content: space-between;
   }
</style>
@endsection
@section('content')
<section class="section">
   <div class="create">
      <div class="row" style="margin-bottom: 20px;">
         <div class="col-md-6">
            <h1>Customers</h1>
         </div>
         <div class="col-md-6" style="text-align: right;">
            <a href="{{url('customer/create')}}" class="btn btn-primary btn-sm" title="Create">
               <i class="fa fa-plus"></i> Create
            </a>
         </div>
      </div>
   <div class="customer">
      <div>
         <table id="example" class="table table-striped table-bordered users__table">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Gender</th>
                  <th>Age</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               @php($i=1)
               @foreach ($customers as $c)
               <tr>
                  <td>{{$i++}}</td>
                  <td>{{$c->name}}</td>
                  <td>{{$c->email}}</td>
                  <td>{{$c->phoneNumber}}</td>
                  <td>{{$c->gender}}</td>
                  <td>{{$c->age}}</td>
                  <td>
                     <a href="{{url('customer/delete', $c->id)}}" class="text-danger" title="Delete"
                        onclick="return confirm('Are you sure to delete?')">
                     <i class="fa fa-trash"></i>
                     </a>
                     &nbsp;&nbsp;
                     <a href="{{route('customer.edit', $c->id)}}" class="text-success">
                     <i class="fa fa-edit"></i>
                     </a>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</section>
@stop
