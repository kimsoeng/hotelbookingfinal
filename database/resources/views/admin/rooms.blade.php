@extends('layouts.admin')
@section('style')
<style>
.room__btn__add {
   margin-bottom: 40px;
}

.room__table {
   width: 100%;
}

.item__booking {
   background-color:#ff3333; 
   border-radius:10px; 
   border: 1px solid #ff3333; 
   padding:4px; 
   font-size:10px; 
   height:30px; 
   text-align:center
}

.item__available {
   background-color:#33cc00; 
   border-radius:10px; 
   border: 1px solid #33cc00; 
   padding:4px; 
   font-size:10px; 
   height:30px; 
   text-align:center;
}

.item__booking__status {
   color: white;
}

.item__action {
   width: 140px;
}

.item__main__action {
   display:flex; 
   justify-content:space-between;
}
</style>
@endsection

@section('content')            
<section class="section">
   <div class="room">
      <div class="room__btn__add">
         <button type="button" class="btn btn-primary">Add</button>
      </div>
      <table id="example" class="table table-striped table-bordered room__table">
         <thead>
            <tr>
               <th>ID</th>
               <th>Room Number</th>
               <th>Room Type</th>
               <th>material </th>
               <th>Status</th>
               <th>Action</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td>1</td>
               <td>1</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__booking">
                     <p class="item__booking__status">Booking</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
            <tr>
               <td>1</td>
               <td>31</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__available">
                     <p class="item__booking__status">Available</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
            <tr>
               <td>1</td>
               <td>21</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__booking">
                     <p class="item__booking__status">Booking</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
            <tr>
               <td>1</td>
               <td>19</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__available">
                     <p class="item__booking__status">Available</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
            <tr>
               <td>1</td>
               <td>12</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__booking">
                     <p class="item__booking__status">Booking</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
            <tr>
               <td>1</td>
               <td>111</td>
               <td>2 beds</td>
               <td>TV, Hair dry, ...</td>
               <td>
                  <div class="item__available">
                     <p class="item__booking__status">Available</p>
                  </div>
               </td>
               <td class="item__action">
                  <div class="item__main__action">
                     <button type="button" class="btn btn-outline-warning">Edit</button>
                     <button type="button" class="btn btn-outline-danger">Delete</button>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</section>
@stop