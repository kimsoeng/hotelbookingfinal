@extends('layouts.admin')
@section('style')
    <style>

    </style>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <h1>Contact Us</h1><br>
            <form method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="toolbox">
                    <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
                        <i class="fa fa-save "></i> Save
                    </button>
                    <br>
                </div>
                <br>
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-gray" style="padding: 57px">
                    <div class="card-block">
                        <div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-4 form-control-label">Name <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Enter your name" value="{{old('name')}}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Type" class="col-sm-4 form-control-label">Email <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="type" name="email"
                                           placeholder="Enter your email" value="{{old('email')}}" required
                                           autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Type" class="col-sm-4 form-control-label">Phone number <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="type" name="phone"
                                           placeholder="Enter your phone" value="{{old('phone')}}" required
                                           autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
