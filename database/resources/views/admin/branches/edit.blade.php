@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Edit Branch</h1>
    <form action="{{route('branch.update', $branch->id)}}" method="POST">
      {{ method_field('PUT') }}
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{route('branch.index')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          {{csrf_field()}}
          <div class="form-group row">
            <label for="branchName" class="col-sm-4 form-control-label">Branch Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="branchName" name="branchName" placeholder="Enter your branch name" value="{{$branch->branchName}}" required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="Type" class="col-sm-4 form-control-label">Short Description</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="shortDescription" name="shortDescription" placeholder="Enter your short description" value="{{$branch->shortDescription}}" required autofocus>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection
