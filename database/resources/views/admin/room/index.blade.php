@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
    <div class="row" style="margin-bottom: 20px;">
      <div class="col-md-6">
        <h1>Room</h1>
      </div>
      <div class="col-md-6" style="text-align: right;">
        <a href="{{route('room.create')}}" class="btn btn-primary btn-sm" title="Create">
          <i class="fa fa-plus"></i> Create
        </a>
      </div>
    </div>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
        <tr>
          <th>Id</th>
          <th>Room No</th>
          <th>Room Type</th>
          <th>Rate</th>
          <th>Price</th>
          <th>Branch</th>
          <th>Adult</th>
          <th>Child</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        @foreach($rooms as $room)
        <tr>
          <td>{{$i++}}</td>
          <td>{{$room->roomNo}}</td>
          <td>{{$room->roomType}}</td>
          <td>{{$room->rate}}</td>
          <td>{{$room->price}}</td>
          <td>{{$room->branch}}</td>
          <td>{{$room->adult}}</td>
          <td>{{$room->child}}</td>
          <td class="action">
            <a href="{{url('room/delete', $room->id)}}" title="Delete" class='text-danger' onclick="return confirm('You want to delete?')">
              <i class="fa fa-trash"></i>
            </a>&nbsp;
            <a href="{{route('room.edit', $room->id)}}" class="text-success" title="Edit">
              <i class="fa fa-edit"></i>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</section>
@endsection

@section('js')
<script>
  $(document).ready(function() {
    $('#example').DataTable();
  });
  // Disable form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@endsection