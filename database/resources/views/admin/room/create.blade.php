@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Create Room</h1>
    <form action="{{route('room.store')}}" method="POST">
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{route('room.index')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          {{csrf_field()}}
          <div class="form-group row">
            <label for="roomNo" class="col-sm-4 form-control-label">Room No <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="roomNo" name="roomNo" placeholder="room number" value="{{old('roomNo')}}" required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="roomTypeId" class="col-sm-4 form-control-label">Room Type</label>
            <div class="col-sm-8">
              <select class="form-control" name="roomTypeId" id="roomTypeId">
                @foreach($roomTypes as $roomType)
                <option value="{{$roomType->id}}">{{$roomType->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="rate" class="col-sm-4 form-control-label">Rate</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" value="{{old('rate')}}">
            </div>
          </div>
          <div class="form-group row">
            <label for="price" class="col-sm-4 form-control-label">Price <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="price" name="price" placeholder="Price" value="{{old('roomNo')}}" required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="branchId" class="col-sm-4 form-control-label">Branch</label>
            <div class="col-sm-8">
              <select class="form-control" name="branchId" id="branchId">
                <option value="">select branch</option>
                @foreach($branches as $branch)
                <option value="{{$branch->id}}">{{$branch->branchName}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="adult" class="col-sm-4 form-control-label">Adult</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="adult" name="adult" placeholder="Adult" value="{{old('adult')}}">
            </div>
          </div>
          <div class="form-group row">
            <label for="child" class="col-sm-4 form-control-label">Child</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" id="child" name="child" placeholder="Child" value="{{old('child')}}">
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection

@section('js')
<script>
  // Disable form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@endsection