@extends('layouts.admin')
@section('style')
<style>

</style>
@endsection

@section('content')
<section class="section">
  <div class="container">
    <h1>Create Room Type</h1>
    <form action="{{route('roomType.store')}}" method="POST">
      <div class="toolbox">
        <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
          <i class="fa fa-save "></i> Save</button>
        <a href="{{route('roomType.index')}}" class="btn btn-warning btn-oval btn-sm">
          <i class="fa fa-reply"></i> Back</a>
      </div>
      <br>
      @if(session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="card card-gray" style="padding: 57px">
        <div class="card-block">
          {{csrf_field()}}
          <div class="form-group row">
            <label for="Name" class="col-sm-4 form-control-label">Name <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{old('name')}}" required autofocus>
            </div>
          </div>
          <div class="form-group row">
            <label for="Type" class="col-sm-4 form-control-label">Type <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="type" name="type" placeholder="type" value="{{old('type')}}" required autofocus>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection

@section('js')
<script>
  // Disable form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@endsection