@extends('layouts.front')
@section('style')
<style>
  .card-body {
    padding: 10px;
    height: 156px
  }

  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 3px
  }

  .card-room {
    height: 113px;
  }

  .price {
    font-size: 17px;
  }

  .btn-book {
    color: #1cc3b2;
  }
</style>
@endsection
@section('banner')
@include("front.banner")
@endsection
@section('content')
<div class="container">

  {{--section hotel--}}
  <section class="con-hotel">
    {{-- <h1 class="text-center">Home page comming soon</h1> --}}
    <h1 class="text-center"><strong> Our Hotel</strong></h1>
    <div style="margin-top: 30px;"></div>
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6 text-center">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro vel obcaecati quam laboriosam delectus dolores animi d
          olore omnis, optio officia, at laudantium odio dolor? Dolorem natus et nisi cupiditate consectetur.</p>
      </div>
      <div class="col-md-3"></div>
    </div>
    <div style="margin-top: 30px;"></div>
    <div class="row">
      <?php $i = 1; ?>
      @foreach($branch as $br)
      <div class="col-md-4" style="padding-top: 10px;">
        <div class="card">
          <img class="card-img-top" src="{{ asset('storage/'. $br->image) }}" alt="Card image cap" width="100%" style="max-height: 205px;">
          <div class="card-body">
            <h2 class="card-title">{{$br->branchName}}</h2>
            <p class="card-text">{{$br->shortDescription}}</p>
            <a href="#" class="text-brand pull-right">View ...</a>
          </div>
        </div>
      </div>
      @endforeach

      <!-- <div class="col-md-4">
        <div class="card">
          <img class="card-img-top" src="{{ url("storage/images/hotel/banner_siemreap.jpg") }}" alt="Card image cap" width="100%">
          <div class="card-body">
            <h2 class="card-title">Siem Reap</h2>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="text-brand pull-right">View ...</a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <img class="card-img-top" src="{{ url("storage/images/hotel/banner_phnompenh.jpg") }}" alt="Card image cap" width="100%">
          <div class="card-body">
            <h2 class="card-title">Preahsihaknouk Ville</h2>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" class="text-brand pull-right">View ...</a>
          </div>
        </div>
      </div> -->
    </div>
  </section>
  {{--end section hotel--}}

  {{--section gallery--}}
  <div style="margin-top: 150px;"></div>
  <section class="con-galery">
    <h1 class="text-center"><strong>OUR ROOM</strong></h1>
    <div class="row">
      <div id="myBtnContainer" class="text-center">
        <button class="btn active btn-default btn-search" onclick="filterSelection('all')"> Show all</button>
        <?php $i = 1; ?>
        @foreach($branch as $br)
        <button class="btn btn-default btn-search" onclick="filterSelection('nature')"> {{$br->branchName}}</button>
        @endforeach
        <!--
        <button class="btn btn-default btn-search" onclick="filterSelection('cars')"> Siem Reap</button>
        <button class="btn btn-default btn-search" onclick="filterSelection('people')"> Sihanouk Ville</button> -->
      </div>
      <div style="margin-top: 30px;"></div>
      <!-- Portfolio Gallery Grid -->
      <div class="row">
        <div class="con-pp">
          <div class="row">
            <?php $i = 1; ?>
            @foreach($rooms as $room)
            <div class="col-md-4" sty>
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> {{$room->roomType}}<small> | {{$room->branch}}</small></h4>
                  <p> A room assigned to one person</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> {{$room->price}} $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="pull-right text-brand">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

            <!-- <div class="col-md-4">
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> Double Room<small> | Phnom Penh</small></h4>
                  <p> A room assigned to two people</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> 25 $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="text-brand pull-right">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> Triple Room<small> | Phnom Penh</small></h4>
                  <p>A room assigned to three people</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> 25 $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="text-brand pull-right">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <!-- <div style="margin-top: 30px;"></div>
          <div class="row">
            <div class="col-md-4">
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> Quad Room<small> | Phnom Penh</small></h4>
                  <p>A room assigned to four people</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> 25 $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="text-brand pull-right">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> Queen Room<small> | Phnom Penh</small></h4>
                  <p> A room with a queen-sized bed</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> 25 $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="btn-book pull-right">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <img class="card-img-top" src="{{ url("storage/images/hotel/room.jpg") }}" alt="Card image cap" width="100%">
                <div class="card-body card-room">
                  <h4 class="card-title"> King Room<small> | Phnom Penh</small></h4>
                  <p>A room with a king-sized bed</p>
                  <div class="row">
                    <div class="col-md-6">
                      <p class="card-text price"><strong class="text-danger">Price:</strong> 25 $</p>
                    </div>
                    <div class="col-md-6"><a href="{{ url('booking') }}" class="btn-book pull-right">Book Now</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        </div>
  </section>
  {{--end section gallery--}}
</div>
@endsection
@section('js')
<script>
  filterSelection("all")

  function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("column");
    if (c == "all") c = "";
    for (i = 0; i < x.length; i++) {
      w3RemoveClass(x[i], "show");
      if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
    }
  }

  function w3AddClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
      if (arr1.indexOf(arr2[i]) == -1) {
        element.className += " " + arr2[i];
      }
    }
  }

  function w3RemoveClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
      while (arr1.indexOf(arr2[i]) > -1) {
        arr1.splice(arr1.indexOf(arr2[i]), 1);
      }
    }
    element.className = arr1.join(" ");
  }


  // Add active class to the current button (highlight it)
  var btnContainer = document.getElementById("myBtnContainer");
  var btns = btnContainer.getElementsByClassName("btn");
  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
    });
  }
</script>
@endsection
