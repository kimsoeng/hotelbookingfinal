@extends('layouts.auth')

@section('content')
<div class="col-md-3"></div>
<div class="col-md-6 con-form">
  <h2 class="text-center"><strong>Login</strong></h2> <br>
  <div class="panel">
    <div class="panel-body">
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group row">
          <label for="email" class="col-md-3">{{ __('Email') }}</label>
          <div class="col-md-9">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail address" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
        <div class="form-group row">
          <label for="password" class="col-md-3">{{ __('Password') }}</label>
          <div class="col-md-9">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-danger pull-left"><a class="btn-cancel" href="{{ url('home') }}">Cancel</a></button>
          <button type="submit" class="btn btn-primary pull-right">
            {{ __('Login') }}
          </button>
        </div>
        <div class="form-group">
          <br><br><br>
          @if (Route::has('register'))
              <p class="pull-right">Don't have account yet? <a class="nav-link" href="{{ route('register') }}">Register here</a></p>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>
<div class="col-md-3"></div>
@endsection