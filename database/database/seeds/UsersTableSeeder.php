<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();
      DB::table('users')->insert([
        'name' => 'Admin',
        'fullname'=>'admin',
        'email' => 'admin@gmail.com',
        'password' => bcrypt('12345678')
      ]);
    }
}
