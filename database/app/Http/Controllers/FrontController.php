<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class FrontController extends Controller
{
  public function index()
  {
    $data['branch'] = DB::table('branches')->get();
    $data['rooms'] = DB::table('rooms')
      ->join('room_types', 'rooms.roomTypeId', '=', 'room_types.id')
      ->join('branches', 'rooms.branchId', '=', 'branches.id')
      ->select('rooms.*', 'room_types.name as roomType', 'branches.branchName as branch')
      ->get();
    return view('front.pages.home', $data);
  }
  public function about()
  {
    return view('front.pages.about');
  }
  public function booking()
  {
    return view('front.pages.booking');
  }
  public function contact()
  {
    return view('front.pages.contact');
  }
  public function career()
  {
    return view('front.pages.career');
  }
  public function careerDetail()
  {
  return view('front.pages.career-detail');
  }
  public function promotion()
  {
    return view('front.pages.promotion');
  }
}
