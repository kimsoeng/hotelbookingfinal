<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use DB;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['branches'] = DB::table('branches')->get();
        return view('admin.branches.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin/branches/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('/uploads');
        }
        $branch = Branch::create([
            'branchName' => $request->branchName,
            'shortDescription' => $request->shortDescription,
            'image' => $image
        ]);
        $i = $branch->save();
        if ($i) {
            return redirect()->route('branch.create')
                ->with('success', 'Data has been saved!');
        } else {
            return redirect()->route('branch.create')
                ->with('error', 'Fail to save data!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['branch'] = Branch::find($id);
        return view('admin/branches/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $branchName = $request->input('branchName');
        $shortDescription = $request->input('shortDescription');
        DB::update('update branches set branchName = ?,shortDescription=?,image=? where id = ?',[$branchName,$shortDescription,$image,$id]);
        return redirect()->route('branch.index', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $i = Branch::where('id', $id)->delete();
        if ($i) {
            if ($i) {
                return redirect()->route('branch.index')
                    ->with('success', 'Data has been deleted!');
            } else {
                return redirect()->route('branch.index')
                    ->with('error', 'Fail to delete data!');
            }
        }
    }
}
