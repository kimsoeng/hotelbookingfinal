<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $data['users'] = DB::table('users')->get();
    return view('admin.users.index', $data);
  }

  public function contact()
  {
   return view('admin.contact.index');
  }

  public function report()
  {
   return view('admin.report');
  }

  public function create()
  {
   return view('admin.users.create');
  }

  public function save(Request $r)
  {
      $data = array(
          'name' => $r->name,
          'email' => $r->email,
          'password' => bcrypt($r->password),
          'fullname'=> $r->fullname
      );
      $i = DB::table('users')->insert($data);
      if($i)
      {
          $r->session()->flash('success', 'Data has been saved!');
          return redirect('/admin/create');
      }
      else{
          $r->session()->flash('error','Fail to save data, please check again!');
          return redirect('admin/create')->withInput();
      }
  }

  public function edit(Request $r)
  {
    $data['users'] = DB::table('users')
            ->where('id', $r->id)
            ->get();
   return view('admin.users.edit', $data);
  }

  public function update(Request $r, $id) {
    $name = $r->input('name');
    $fullname = $r->input('fullname');
    $email = $r->input('email');
    $password = $r->input('password');
    $i = DB::update('update users set name = ?,fullname=?,email=?,password=? where id = ?',[$name,$fullname,$email,$password,$id]);

    return redirect('/admin');
  }


  public function delete(Request $r) {
    DB::table('users')
        ->where('id', $r->id)
        ->delete();
        $r->session()->flash('success', 'Data has been removed!');
      return redirect('/admin');
  }
}
