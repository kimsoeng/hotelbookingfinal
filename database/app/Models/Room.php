<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'roomNo', 'roomTypeId', 'rate', 'price', 'branchId', 'adult', 'child',
    ];
}
